<?php
    // Session management
    include_once('../../../business/php/session.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Home - Perle News</title>
    </head>
    <body class="color--primary">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <?php
                        include_once("../../../presentation/php/news-tiles.php");
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>