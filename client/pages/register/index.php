<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
<link rel="stylesheet" href="../../../presentation/css/index.css">
<script type="text/javascript" src="../../../business/js/register.js"></script>
</head>

<body>
    <title>Register</title>
    <form action="../../../business/php/register" onsubmit="return validateData()" method="POST">
        <div class="container" style="text-align: center;">
            <h1>Register</h1>
            <p>All fileds are required</p>
            <label for="username"><b>Username</b></label>
            <input type="text" maxlength="50" placeholder="Enter Username" name="username" id="username">
            <p id="username-error"></p>
            <label for="email"><b>Username</b></label>
            <input type="text" maxlength="50" placeholder="Enter Email" name="email" id="email">
            <p id="email-error"></p>
            <label for="password"><b>Password</b></label>
            <input type="password" maxlength="72" placeholder="Enter Password" name="password" id="password">
            <p id="password-error"></p>
            <label for="confirm_password"><b>Repeat Password</b></label>
            <input maxlength="72" type="password" placeholder="Repeat Password" name="confirm_password" id="confirm_password">
            <p id="passwordrepeat-error"></p>
            <button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo $_SERVER['HTTP_REFERER'] ?>'"> Cancel</button> <button type="submit" class="btn btn-primary">Register</button>
        </div>
        <div class="container signin" style="text-align: center;">
            <p>Already have an account? <a href="../login/login.php">Sign in</a>.</p>
        </div>
    </form>
    <script>
        
    </script>
</body>

</html>