<!DOCTYPE html>
<html> 
<head>
<link rel="stylesheet" href="../../../presentation/css/index.css">
</head> 
<body>
<title>Login</title>

<div class="container" style="text-align: center;">
 <h1>Login</h1>
<form action="../../../business/php/login" method="post">
Username:<br>
<input type="text" placeholder='Username' size="40" maxlength="250" name="username" id="username"><br><br>
 
Password:<br>
<input type="password" placeholder='Password' size="40"  maxlength="250" name="password" id="password"><br>

<button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo $_SERVER['HTTP_REFERER'] ?>'"> Cancel</button> <button type="submit" class="btn btn-primary">Submit</button>
<p>Don't have an account <a href="../../../client/pages/register/index.php">register now</a></p> 
</div>

</body> 
</html>