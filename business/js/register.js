let isvalid = false;
const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        function validateData() {

            let username = document.getElementById("username").value;
            let password = document.getElementById("password").value;
            let email = document.getElementById("email").value;
            let confirmpassword = document.getElementById("confirm_password").value;
            if (username != "" || password != "" || confirmpassword != "") {
                if (username.length < 5) {
                    document.getElementById("username-error").innerHTML = "Username is too short. Must be  5 characters or more and not more than 50 characters.";
                    isvalid = false;
                }
                // else if (username.length > 4) 
                // {
                //     let xmlhttp = new XMLHttpRequest();
                //     xmlhttp.onreadystatechange = function() {
                //         if (this.readyState == 4 && this.status == 200) {
                //             if (this.responseText > 0) {
                //                 isvalid = false;
                //                 document.getElementById("username-error").innerHTML = "This username is already taken";
                //             }
                //         }
                //     };
                //     console.log(username);
                //     xmlhttp.open("GET", "/../api/usernameAvailable.php?username=" + username, false);
                //     xmlhttp.send();
                // }
                // else {
                //     document.getElementById("username-error").innerHTML = null;
                // }
                if (password.length < 8) {
                    document.getElementById("password-error").innerHTML = "Password is too short. Must be  8 characters or more and not more than 72 characters.";
                    isvalid = false;
                } else {
                    document.getElementById("password-error").innerHTML = null;
                }
                if (re.test(String(email).toLowerCase()) == false) {
                    document.getElementById("email-error").innerHTML = "Email is invalid";
                    isvalid = false;
                } else {
                    document.getElementById("email-error").innerHTML = null;
                }
                if (password != confirmpassword) {
                    document.getElementById("passwordrepeat-error").innerHTML = "Passwords dont match!"
                    isvalid = false;
                } else {
                    document.getElementById("passwordrepeat-error").innerHTML = null;
                }
                if (password == confirmpassword && password.length > 7 && username.length > 4) {
                    isvalid = true;
                }
            }
            return isvalid;
        }