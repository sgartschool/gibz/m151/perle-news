<?php
include "../../business/php/usernameAvailable.php";
include  "../../presentation/php/classes/user.php";
$username = $password = $confirm_password = $email = "";
$username_err =$email_err = $password_err = $confirm_password_err = "";
$user = new User();
// Processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Validate username
    if (empty(trim($_POST["username"]))) {
        $username_err = "Please enter a username.";
    } 
    elseif (strlen(trim($_POST["username"])) < 3 || strlen(trim($_POST["username"])) > 50) {
        $username_err = "Username must have atleast 3 characters and not more than 50 characters..";
    } 
    else {
        // Set parameters
        $param_username = trim($_POST["username"]);
        
        if ( checkUsernameAvaiability($param_username) > 0) {
            $username_err = "This username is already taken.";
        } else {
            $username = trim($_POST["username"]);
        }
    }

    // Validate password
    if (empty(trim($_POST["password"]))) {
        $password_err = "Please enter a password.";
    } elseif (strlen(trim($_POST["password"])) < 8 || strlen(trim($_POST["password"])) > 72) {
        $password_err = "Password must have atleast 8 characters and not more than 72 characters.";
    } else {
        $password = trim($_POST["password"]);
    }


// Validate email
if (empty(trim($_POST["email"]))) {
    $email_err = "Please enter a email.";
} elseif (!filter_var(trim($_POST["email"]), FILTER_VALIDATE_EMAIL)) {
    $email_err = "Email invalid";
} else {
    $email = trim($_POST["email"]);
}


    // Validate confirm password
    if (empty(trim($_POST["confirm_password"]))) {
        $confirm_password_err = "Please confirm password.";
    } else {
        $confirm_password = trim($_POST["confirm_password"]);
        if (empty($password_err) && ($password != $confirm_password)) {
            $confirm_password_err = "Password did not match.";
        }
    }

    // Check input errors before inserting in database
    if (empty($email_err) && empty($username_err) && empty($password_err) && empty($confirm_password_err)) {

        $param_username = $username;
        $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash

        $user->createUser($param_username,$param_password,$param_email);
        echo "<script>location.href='../../../client/pages/login';</script>";
    } 
    else {
        echo $username_err;
        echo $password_err;
        echo $confirm_password_err;
    }
}
?>

<a href="../../client/pages/register/index.php">Register</a>