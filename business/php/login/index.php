<?php
    if (isset($_SESSION["loggedIn"])){
        header("Location: ../../../client/pages/home");
        exit();
    }

    if (isset($_POST["username"]) && isset($_POST["password"])) {
        session_start();
        include("../../../presentation/php/classes/user.php");
        $user = new User();
        if ( $user->checkLogin($_POST["username"], hash('sha256', $_POST["password"]))) {
            $_SESSION["username"] = $login["user"]["username"];
            $_SESSION["userId"] = $login["user"]["userId"];
            if (isset($login["user"]["email"])) {
                $email = $login["user"]["email"];
            }
            $_SESSION["loggedIn"] = true;
            $_SESSION["ipAddress"] = $_SERVER["REMOTE_ADDR"];
            $_SESSION["userAgent"] = $_SERVER["HTTP_USER_AGENT"];
            $_SESSION["lastAccess"] = time();
            Header("Location: ../../../client/pages/home?toast=".$login["success"]."&message=".$login["message"]);
            exit();
        } else {
            session_destroy();
            Header("Location: ../../../client/pages/login?success=false&message=" . $login["message"]);
            exit();
        }
    }
    else {
        header("Location: ../../../client/pages/login?success=false&message=Error");
        exit();
    }
?>
