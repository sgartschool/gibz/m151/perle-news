<?php
    session_start();

    // IP Address match
    if (!isset($_SESSION["ipAddress"]) || $_SERVER["REMOTE_ADDR"] != $_SESSION["ipAddress"])
        session_destroy();

    // User Agent match
    if (!isset($_SESSION["userAgent"]) || $_SERVER["HTTP_USER_AGENT"] != $_SESSION["userAgent"])
        session_destroy();

    // Last access older than 1h
    if (!isset($_SESSION["lastAccess"]) || time() >  ($_SESSION["lastAccess"] + 3600))
        session_destroy();
    else
        $_SESSION["lastAccess"] = time();