<?php
class Database {
    
    // Change database file in config/database.php

    private PDO $connection;

    public function __construct(){
        require_once('../../../config/database.php');
        if (isset($file)){
            $this->connection = new PDO("sqlite:$file") or die("Failed to connect to database");
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        else {
            die("Failed to connect to database: Missing credentials! <br><a href='../../../client/pages/home'>Back to homepage</a>");
        }
    }

    public function read(string $query, array $params = null, int $limit = null) {
        $statement = $this->connection->prepare($query);
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (isset($limit)){
            $query = $query." LIMIT :limit";
            $statement->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        }
        if (isset($params)) {
            $statement->execute($params);
        } else {
            $statement->execute();
        }
        return $statement->fetchAll();
    }

    public function write(string $query, array $params = null) {
        $statement = $this->connection->prepare($query);
        if (isset($params)) {
            $statement->execute($params);
        } else {
            $statement->execute();
        }
    }

}
