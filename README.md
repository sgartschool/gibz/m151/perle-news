# Perle News

**[View here](https://perlenews.sgart.io)**

## Changelog

### 2021-06-07

| Change | Contributor | Issue |
| :----- | :---------: | :---- |
| Add themes | [sgart](https://git.sgart.io/sgart) | [#1](https://git.sgart.io/gibz/m151/perle-news/-/issues/1) |
| Add Materializecss framework | [sgart](https://git.sgart.io/sgart) | [#1](https://git.sgart.io/gibz/m151/perle-news/-/issues/1) |
| Create database | [lgattiker](https://git.sgart.io/lgattiker) | [#2](https://git.sgart.io/gibz/m151/perle-news/-/issues/2) |
| Add CI/CD | [sgart](https://git.sgart.io/sgart) | [#3](https://git.sgart.io/gibz/m151/perle-news/-/issues/3) |
| Add uptime tracking | [sgart](https://git.sgart.io/sgart) | [#5](https://git.sgart.io/gibz/m151/perle-news/-/issues/5)

### 2021-06-14

| Change | Contributor | Issue |
| :----- | :---------: | :---- |
| Add homepage | [sgart](https://git.sgart.io/sgart) | [#8](https://git.sgart.io/gibz/m151/perle-news/-/issues/8) |
| Move from 3-Tier to 4-Tier architecture | [sgart](https://git.sgart.io/sgart) | - |
| Add changelog | [sgart](https://git.sgart.io/sgart) | [#7](https://git.sgart.io/gibz/m151/perle-news/-/issues/7) |
| Add login | [lgattiker](https://git.sgart.io/lgattiker) | [#6](https://git.sgart.io/gibz/m151/perle-news/-/issues/6) |
| Add logout | [lgattiker](https://git.sgart.io/lgattiker) | - |

### 2021-06-16

| Change | Contributor | Issue |
| :----- | :---------: | :---- |
| Change .gitignore to exclude vscode files | [sgart](https://git.sgart.io/sgart) | - |

### 2021-06-21

| Change | Contributor | Issue |
| :----- | :---------: | :---- |
| Add php linting to pipeline | [sgart](https://git.sgart.io/sgart) | [#11](https://git.sgart.io/gibz/m151/perle-news/-/issues/11) |
| Edit homepage | [sgart](https://git.sgart.io/sgart) | [#8](https://git.sgart.io/gibz/m151/perle-news/-/issues/8) |
| Create news tiles | [sgart](https://git.sgart.io/sgart) | [#12](https://git.sgart.io/gibz/m151/perle-news/-/issues/12) |
| Create post backend | [sgart](https://git.sgart.io/sgart) | [#13](https://git.sgart.io/gibz/m151/perle-news/-/issues/13) |
| Create user backend | [sgart](https://git.sgart.io/sgart) | [#14](https://git.sgart.io/gibz/m151/perle-news/-/issues/14) |
| Create php classes | [lgattiker](https://git.sgart.io/lgattiker) | [#10](https://git.sgart.io/gibz/m151/perle-news/-/issues/10) |

### 2021-06-23

| Change | Contributor | Issue |
| :----- | :---------: | :---- |
| Update .gitignore to exclude JetBrains files | [sgart](https://git.sgart.io/sgart) | - |
| Replace theming scripts with cdn version | [sgart](https://git.sgart.io/sgart) | - |
| Add register files | [lgattiker](https://git.sgart.io/lgattiker) | [#16](https://git.sgart.io/gibz/m151/perle-news/-/issues/16) |


### 2021-06-26

| Change | Contributor | Issue |
| :----- | :---------: | :---- |
| Buisnesslogic for Login | [lgattiker](https://git.sgart.io/lgattiker) | [#19](https://git.sgart.io/gibz/m151/perle-news/-/issues/19) |
| Buisnesslogic for Register | [lgattiker](https://git.sgart.io/lgattiker) | [#18](https://git.sgart.io/gibz/m151/perle-news/-/issues/18) |

### 2021-06-27
| Change | Contributor | Issue |
| :----- | :---------: | :---- |
| Prevent session hijacking | [sgart](https://git.sgart.io/sgart) | [#20](https://git.sgart.io/gibz/m151/perle-news/-/issues/20) |

### 2021-06-28
| Change | Contributor | Issue |
| :----- | :---------: | :---- |
| Fix sessions | [sgart](https://git.sgart.io/sgart) | [#20](https://git.sgart.io/gibz/m151/perle-news/-/issues/20) |
| Fix database paths | [sgart](https://git.sgart.io/sgart) | [#15](https://git.sgart.io/gibz/m151/perle-news/-/issues/15) |
| Fix login | [sgart](https://git.sgart.io/sgart) | [#23](https://git.sgart.io/gibz/m151/perle-news/-/issues/23) |