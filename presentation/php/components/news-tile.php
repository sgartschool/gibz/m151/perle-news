<div class="card big polar-darken-3 hoverable" id="<?= $post["id"] ?>">
    <div class="card-content color--secondary__text">
        <a href="../user/<?= $post["user"]["username"] ?>" class="frost-text-cyan"><?= $post["user"]["username"] ?></a>
        <span class="card-title color--primary__text"><?= $post["title"] ?></span>
        <p class="truncate"><?= $post["description"] ?></p>
    </div>
    <div class="card-action">
        <a href="../posts/upvote.php?id=<?= $post["postId"] ?>&loc=home"><i class="material-icons <?= $post["hasUpvoted"] ? "aurora-text-green" : "frost-text-cyan" ?>">arrow_upward</i><span class="<?= $post["hasUpvoted"] ? "aurora-text-green" : "frost-text-cyan" ?>"><?= $post["upvotes"] ?></span></a>
        <a href="../posts/downvote.php?id=<?= $post["postId"] ?>&loc=home"><i class="material-icons <?= $post["hasDownvoted"] ? "aurora-text-red" : "frost-text-cyan" ?>">arrow_downward</i><span class="<?= $post["hasDownvoted"] ? "aurora-text-red" : "frost-text-cyan" ?>"><?= $post["downvotes"] ?></span></a>
    </div>
</div>