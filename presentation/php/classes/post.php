<?php
class Post {
    private $database;
    private $userBackend;

    private $id;
    private $content;
    private $title;
    private $user;
    private $createdDate;

    public function __construct(int $id = null, string $content = null, string $title = null, int $userId = null, DateTime $createdDate = null){
        require_once("../../../dataaccess/database.php");
        $database = new Database();
        require_once("../../../presentation/php/classes/user.php");
        $user = new User();
        $this->database = $database;
        $this->userBackend = $user;
        if (isset($id, $content, $title, $userId, $createdDate)){
            $this->id = $id;
            $this->content = $content;
            $this->title = $title;
            $this->user = $user->getUserById($userId);
            $this->createdDate = $createdDate;
        }
    }

    public function getAllPosts(){
        return $this->database->read("SELECT * FROM posts");
    }

    public function getPostById($id){
        $data = $this->database->read("SELECT * FROM post WHERE id = ?", [$id]);
        return array (
            "id" => $data["id"],
            "title" => $data["title"],
            "content" => $data["content"],
            "user" => $this->userBackend->getUserById($data["userId"]),
            "createdDate" => $data["createdDate"]
        );
    }
}
