<?php
class User {
    private $database;
    
    private $id;
    private $username;
    private $isAuthor;
    private $password;
    private $email;

    public function __construct(int $id = null, string $username = null, bool $isAuthor = null, string $password = null, string $email = null){
        require_once("../../../dataaccess/database.php");
        $database = new Database();
        require_once("../../../presentation/php/classes/user.php");
        $user = new User();
        $this->database = $database;
        $this->userBackend = $user;
        if (isset($id, $username, $isAuthor, $password, $email)){
            $this->id = $id;
            $this->username = $username;
            $this->isAuthor = $isAuthor;
            $this->password = $password;
            $this->email = $email;
        }
    }

    public function getUserById($id){
        return $this->database->read("SELECT * FROM user WHERE id = ?", [$id]);
    }
    public function getUserByUsername($username){
        return $this->database->read("SELECT * FROM user WHERE username = ?", [$username]);
    }
    public function createUser($param_username,$param_password,$param_email){
        return $this->database->write("INSERT INTO users (username, IsAuthor, password,email) VALUES (?,?,?,?)", [$param_username,0,$param_password,$param_email]);
    }

    public function checkLogin($username,$password)
    {
      $foundUser = $this->getUserByUsername($username);
      return password_verify($password, $foundUser['password']);
    }
}
